# Sign Up beLike  README 

[- Español](#Español)  
[- English](#English) 

# Español

Este proyecto es una prueba técnica para beLike. 

Los criterios de valoración eran los siguientes: 

● Resolver el problema de la forma más simple y con el menor código posible.  
● Que se pueda ejecutar sin tu ayuda  
● Que el código comunique bien su intención


## Clonar proyecto
El primer paso es clonar este proyecto en tu ordenador. Para ello elige la opción *Clone with HTTPS*  en la propia página de gitlab y pega el comando en tu consola.
(No olvides escribir 'git clone' primero) 
 

## Setup del proyecto

La primera vez que ejecutes esta app vas a necesitar instalar algunos paquetes de node sin los cuales no funcionaría. El comando para realizar esto es: 

```
npm install
```

### Compila y haz recarga para desarrollo

Una vez tengas todos los paquetes necesarios, puedes montar la aplicación de forma local lanzando el siguiente comando: 

```
npm run serve
```

Al finalizar el proceso, en consola aparecerá el puerto en el que se ha montado la aplicación. Clickando sobre la dirección mientras mantienes mayúscula apretada, accederás de forma inmediata. 

### Backend
Desde consola  y dentro de la carpeta Api del proyecto, ejecuta 

```
npm install
```
Este comando, al igual que en el frontEnd, instalará los paquetes necesarios. 

Tras esto, ejecuta 

```
npm start
```

Esto alzará el backend. Recuerda que para el uso de la app debes tener a la vez conectados frontend y backend. 

POSIBLE ERROR : Si hay un problema y te aparece el siguiente error : **ReferenceError: TextEncoder is not defined**, busca en archivo Api/NODE_MODULES/whatwg-url/lib/encoding.js y pega este *chunk* en la línea 2. 

```
const {TextDecoder, TextEncoder} = require("util")
```




### Compila y minimiza para producción
```
npm run build
```

### Ejecuta lint y arregla archivos. 
```
npm run lint
```

### Personalizar configuración
See [Configuration Reference](https://cli.vuejs.org/config/).

### Testing

Para el testeo de la aplicación usamos la herramienta de testing *Cypress*
Para lanzar los test ejecuta el siguiente comando: 

```
npm test
```


# English
Disclaimer: This english readme has been written by a non-native english speaker and could contain grammar mistakes. 

This project is a technical test for beLike.

The assesment criteria were as follows:

● Solve the problem in the simplest way and with the least possible code.  
● It can be run without your help  
● The code communicates its intention well


## Clonning the project

The first step is clonning this project in your computer. To this end, please chose the option   *Clone with HTTPS*  at the same gitlab page and paste the command in your console. (Don't forget to type 'git clone' first!)


Once you'd clone the project in your computer: 

## Project setup
The first time you execute this app you are going to need to install some node packages without them this wouldn't work. 
The command to do that is: 

```
npm install
```

### Compiles and hot-reloads for development
Once you have all the necessary packages, you can locally build the app  by typing the next command: 
 
```

npm run serve
```
After the process is completed, you will see the port number where the app is mounted in console. 



### Backend
Desde consola  y dentro de la carpeta Api del proyecto, ejecuta

In you console and inside the Api folder from the project type:

```
npm install
```

This command, like in the frontEnd part, will install the needed packages. 

After that type: 

```
npm start
```
This action will mount the backend. Remember that if you want to use the app, you will need both front and backend operative. 

POSSIBLE MISTAKE : If there is a problem and the following error appears in console: **ReferenceError: TextEncoder is not defined**, look for the file  Api/NODE_MODULES/whatwg-url/lib/encoding.js and paste this *chunk* at  line 2. 

```
const {TextDecoder, TextEncoder} = require("util")
```


### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Testing

In order to test this app we use the *Cypress* testing tool 
To launch the test type the following command in your console: 


```
npm test
```



