describe('Sign Up', () => {


    it('visits our homepage', () => {
        let user = new User()

        user.visitOurHomePage('localhost:8080')

    })

    it('introduces an invalid name', () => {
        let user = new User()
        user.introduceInvalidUserName()
        user.getUserErrorInfo()
    })
})


const HOMEPAGE = 'localhost:8080'
const NAME_INPUT = '[placeholder="username"]'
const INVALID_USERNAME = 'Manolo%'
const ERROR_BOX = 'h5'
class User {
    visitOurHomePage() {
        cy.visit(HOMEPAGE)
    }

    introduceInvalidUserName() {
        cy.get(NAME_INPUT).type(INVALID_USERNAME)
    }

    getUserErrorInfo() {
        cy.get(ERROR_BOX)
            .contains('Your user name must contain only alphanumeric characters')
    }
}
