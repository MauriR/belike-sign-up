const USER = require('../models/Users')


class User {
    async createUser(request, response) {
        try {
 
            const userName = request.body.name
            const existingUser = await USER.findOne({ name: userName })
           
            if (existingUser) {
                return response.status(409).json({ message: `The user ${userName} already exists` })
            }
            const user = await USER.create(request.body)
            response.status(201).json({ user })
        } catch (error) {
            
            response.status(500).json({ message: error })
        }
    }


    getUser(request, response) {
        response.json({ id: request.params.id })
    }
    getAllUsers(request, response) {
        response.send('get all users')
    }

}

module.exports = User









