const connectDB = require('./database/connect')
const express = require('express')
const app = express()
const users = require('./routes/Users')
const cors = require('cors')
require('dotenv').config()


app.use(express.json()) 
app.use(express.static('../public')) 
app.use(cors())
 
app.get('/hello',(request,response)=>{
    response.send('Be Like Sign In App')
})

 app.use('/api/v1/users', users)       
        


const port = 3300

const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI)

        app.listen(port, () => {
            console.log(`Broadcasting from port:  ${port}`)
        })
    } catch (error) {
        console.log(error)
    }
}

start()

