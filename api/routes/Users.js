const express = require('express')
const router = express.Router()
const User = require('../controllers/Users')
const user = new User()


router.route('/').post(user.createUser).get(user.getAllUsers)
router.route('/:id').get(user.getUser)

module.exports= router
