import Vue from "vue";
import Router from 'vue-router'
import Store from '../store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: "/", name: "home", component: () => import('../views/SignUp.vue') },
    {
      path: "/intranet", name: "intranet", component: () => import('../views/Intranet.vue'), meta: {
        requiresUser: true,
      },
    },
  ]
})

function isInIntranetWithNoUser(conditionA, conditionB) {
  return !conditionA && conditionB
}

router.beforeEach(async (to, from, next) => {
  const user = Store.state.user
  const requiresUser = to.matched.some(record => record.meta.requiresUser)
  if (isInIntranetWithNoUser(user, requiresUser)) {
    next({ path: '/' })
  }
  else {
    next();
  }
})

export default router
